<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\MailRepositoryInterface;
use App\Repositories\MailRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MailRepositoryInterface::class, MailRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
