<?php

namespace App\Repositories;

interface AuthRepositoryInterface {
    public function getModel();
    public function getEmail($email);
    public function callbackFromGoogle($id,  $data);
}