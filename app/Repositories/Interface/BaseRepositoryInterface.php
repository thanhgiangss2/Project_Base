<?php

namespace App\Repositories;

interface BaseRepositoryInterface {
    public function getModel();
    public function setModel();
    public function index();
    public function search($data);
    public function create($data);
    public function update($data);
    public function delete($id);
}