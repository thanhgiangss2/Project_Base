<?php

namespace App\Repositories;


interface MailRepositoryInterface {
    public function index();
    public function search($data);
    public function show($id);
    public function store($data);
    public function getReceiver($mail, $id);
    public function saveData($data, $id);
    public function delete($id);
}