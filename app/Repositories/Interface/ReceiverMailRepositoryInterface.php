<?php

namespace App\Repositories;

interface ReceiverMailRepositoryInterface {
    public function index();
    public function search($data);
    public function show($id);
    public function store($data);
    public function getReceiver($mail, $id);
    public function saveData($data, $id);
    public function delete($id);
}