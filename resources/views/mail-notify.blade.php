<!DOCTYPE html>
<html>
<head>
   <style>
      .container {
         text-align: center;
         font-size: 1rem;
         color: #394867;
      }
      .btn {
         color: #fff !important;
         text-decoration: none;
         background-color: #007bff;
         border-color: #007bff;
         display: inline-block;
         font-weight: 400;
         text-align: center;
         white-space: nowrap;
         vertical-align: middle;
         -webkit-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
         border: 1px solid transparent;
         padding: 0.375rem 0.75rem;
         font-size: 1rem;
         line-height: 1.5;
         border-radius: 0.25rem;
         transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
      }
      .btn:active {
         color: #fff;
         background-color: #0062cc;
         border-color: #005cbf;
      }
      .btn:hover {
         color: #fff;
         background-color: #0069d9;
         border-color: #0062cc;
         cursor: pointer;
      }
      /* .btn a {
         color: inherit;
         text-decoration: none;
      } */
   </style>
</head>  
<body>
   <div class="container">
      <span>{!! $mailData['content'] !!}</span>
      <a href="{{ $mailData['url'] }}" class="btn" role="button">TRUY CẬP TRANG</a>
   </div>
</body>
</html>