@extends('layout.app')

@section('content')
    <show-view 
    :title="{{ $mailData['title'] }}"
    :content="{!! $mailData['content'] !!}" />
    <p>{{ $mailData['url'] }}</p>
@endsection